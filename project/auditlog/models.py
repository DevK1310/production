from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
# Create your models here.


class History(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True)
    object_id = models.PositiveBigIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    desc = models.TextField()
    # def __str__(self):
    #     return self.object_id

    # class Meta:
    #     verbose_name_plural = "Histories"
