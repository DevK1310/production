# Generated by Django 3.2.7 on 2021-11-10 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auditlog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='history',
            name='desc',
            field=models.TextField(default='Add'),
            preserve_default=False,
        ),
    ]
