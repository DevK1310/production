from django.urls import path
from . import views
from auditlog.views import *

urlpatterns = [
    path('aa', views.home, name='home'),
    path('history/<str:model_name>/<int:object_id>', views.show_auditlog, name='auditlog'),
    # path('auditlog/', views.auditlog, name='auditlog'),
    # path('history/<str:model>/<int:pk>', views.auditlog, name='auditlog'),
]
