
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function getIP(){
    $.get("https://ipinfo.io", function(response) {
        // alert(response.ip)
        return response.ip
    }, "json")
}

var fun_data = null
$('#saveBank').click(function(){
    // alert("dfsdfhbfh")
    bank_id = $('#bank_hid').val()
    bank_name = $('#bank_name').val()
    acc_name = $('#acc_name').val()
    acc_no = $('#acc_no').val()
    branch_name = $('#branch_name').val()
    isfc_code = $('#isfc_code').val()
    out_bal = $('#out_bal').val()
    if (isfc_code.length != 11){
        alertify.set('notifier','position', 'top-center');
        alertify.error("Invalid isfc code");
        return false
    }
    // alert('/bank/' + bank_hid)
    getIP();    
    $.ajax({
        url: '/bank',
        data: {
            'bank_id' : bank_id,
            'bank_name' : bank_name,
            'acc_name' : acc_name,
            'acc_no' : acc_no,
            'branch_name' : branch_name,
            'isfc_code' : isfc_code,
            'out_bal' : out_bal,
            'csrfmiddlewaretoken' : getCookie("csrftoken"),
        },// get the form data
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            if (data.code == 1){ 
                if (data.code==1){
                    alert(data.bank_id)
                    alertify.set('notifier','position', 'top-center');
                    alertify.success(data.msg);   
                    window.location.href = "http://127.0.0.1:8000/bank/"+data.bank_id
                }
            }
            else if(data.code == 0){
                alertify.set('notifier','position', 'top-center');
                alertify.error(data.invalid_msg);
                return false;
            }
        }
    });
});
