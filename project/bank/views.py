from typing import Counter
from django.db.models.expressions import Exists
from django.http import response
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from bank.models import Bank, Transaction
import decimal
# from django.core.paginator import Paginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from auditlog.views import auditlog
# Create your views here.


def home(request):
    # auditlog.create("model_name","object_id","user_id","desc")
    return render(request, "bank/final.html")

def banks(request):
    banks_list = Bank.objects.all().order_by('-id')
    count = banks_list.count()
    page = request.GET.get("page", 1)
    paginator = Paginator(banks_list, 10)
    try:
        banks = paginator.page(page)
    except PageNotAnInteger:
        banks = paginator.page(1)
    except EmptyPage:
        banks = paginator.page(paginator.num_pages)
        # page_obj = paginator.get_page(page_num)
    response = {
        'banks': banks,
        'count': count,
    }
    return render(request, "bank/banks.html", response)

# not needed
def add_bank(request, id=None):
    user_id = request.user.id
    if request.method == 'POST':
        bank_name = request.POST.get("bank_name")
        acc_no = request.POST.get("acc_no")
        branch_name = request.POST.get("branch_name")
        isfc_code = request.POST.get("isfc_code")
        out_bal = request.POST.get("out_bal")

        exists_bank = Bank.objects.filter(name=bank_name).first()
        if id:
            pass
        else:
            exists_bank = Bank.objects.filter(name=bank_name).first()
            if exists_bank:
                invalid_msg = bank_name + "is already exist."
                print(invalid_msg)
                # return HttpResponse(invalid_msg,code=0)
                return JsonResponse({'invalid_msg': invalid_msg, 'code': 0}, status=200)
            else:
                print(bank_name, " | ", acc_no, " | ", branch_name,
                    " | ", isfc_code, " | ", out_bal, " | ")
                bank_obj = Bank.objects.create(
                    name=bank_name,
                    account_no=int(acc_no),
                    address=branch_name,
                    isfc_code=int(isfc_code),
                    outstanding_bal=decimal.Decimal(out_bal),
                )
                auditlog.create("bank",bank_obj.id,user_id,"Bank added")


            data = {
                "bank_name": bank_name,
                "code": 1,
            }
            return JsonResponse(data, status=200)

        # return render(request, "bank/bank.html")
    if id:
        bank = Bank.objects.get(id=int(id))
        response = {
            'data': bank,
        }
        return render(request, "bank/bank.html", response)

    return render(request, "bank/bank.html")

def bank(request, id=None):
    try:
        if request.method == 'POST':
            user_id = request.user.id
            bank_id = int(request.POST.get("bank_id"))
            bank_name = request.POST.get("bank_name")
            acc_no = request.POST.get("acc_no")
            branch_name = request.POST.get("branch_name")
            isfc_code = request.POST.get("isfc_code")
            out_bal = request.POST.get("out_bal")

            if bank_id != 0:
                current_bank = Bank.objects.filter(id=bank_id).first()
                exists_bank = Bank.objects.filter(name=bank_name).first()
                if exists_bank and current_bank.name != bank_name:
                    invalid_msg = bank_name + " is already exist."
                    return JsonResponse({'invalid_msg': invalid_msg, 'code': 0}, status=200)
                else:
                    bank_obj = Bank.objects.filter(id=bank_id).update(
                        name=bank_name,
                        account_no=int(acc_no),
                        address=branch_name,
                        isfc_code=int(isfc_code),
                        outstanding_bal=decimal.Decimal(out_bal),
                    )
                    auditlog.create("bank",bank_obj,user_id,"Bank updated.")
                    return JsonResponse({"msg": bank_name + " updated", "bank_id":bank_id, "code": 1}, status=200)
            else:
                exists_bank = Bank.objects.filter(name=bank_name).first()
                if exists_bank:
                    invalid_msg = bank_name + " is already exist."
                    return JsonResponse({'invalid_msg': invalid_msg, 'code': 0}, status=200)
                bank_obj = Bank.objects.create(
                    name=bank_name,
                    account_no=int(acc_no),
                    address=branch_name,
                    isfc_code=int(isfc_code),
                    outstanding_bal=decimal.Decimal(out_bal),
                )
                auditlog.create("bank",bank_obj.id,user_id,"Bank added")
                return JsonResponse({"msg": bank_obj.name + " created","bank_id":bank_obj.id, "code": 1}, status=200)
        else:
            if id:
                bank = Bank.objects.get(id=int(id))
                return render(request, "bank/bank.html", {'data': bank})
            else:
                return render(request, "bank/bank.html")

    except Exception as e:
        return JsonResponse({'invalid_msg': "Something went wrong" + str(e), 'code': 0}, status=200)

def delete_banks(request):
    try:
        if request.method == 'POST':
            print(dict(request.POST.items()))
            ids = request.POST.get("ids").split(',')
            bank_name = ''
            # return JsonResponse({'msg': 'Something went wrong ', 'code': 0},status=200)
            for id in ids:
                bank_obj = Bank.objects.get(id=id)
                bank_name = bank_name + ',' + bank_obj.name if bank_name != '' else bank_name
                bank_obj.delete()
            return JsonResponse({'msg': 'Bank "' + bank_name + '" deleted.', 'code': 1}, status=200)
    except Exception as e:
        return JsonResponse({'msg': 'Something went wrong ', 'code': 0}, status=200)

def transactions(request):
    transactions_list = Transaction.objects.all().order_by('-id')
    count = transactions_list.count()
    page = request.GET.get("page", 1)
    paginator = Paginator(transactions_list, 10)
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        transactions = paginator.page(1)
    except EmptyPage:
        transactions = paginator.page(paginator.num_pages)
        # page_obj = paginator.get_page(page_num)
    response = {
        'transactions': transactions,
        'count' : count,
    }
    return render(request, "bank/transactions.html", response)
    # return render(request, "bank/transcation.html")

def transaction(request, id=None):
    try:
        if request.method == 'POST':
            user_id = request.user.id
            bank_id = int(request.POST.get("bank_id"))
            bank_name = request.POST.get("bank_name")
            acc_no = request.POST.get("acc_no")
            branch_name = request.POST.get("branch_name")
            isfc_code = request.POST.get("isfc_code")
            out_bal = request.POST.get("out_bal")

            if bank_id != 0:
                current_bank = Bank.objects.filter(id=bank_id).first()
                exists_bank = Bank.objects.filter(name=bank_name).first()
                if exists_bank and current_bank.name != bank_name:
                    invalid_msg = bank_name + " is already exist."
                    return JsonResponse({'invalid_msg': invalid_msg, 'code': 0}, status=200)
                else:
                    bank_obj = Bank.objects.filter(id=bank_id).update(
                        name=bank_name,
                        account_no=int(acc_no),
                        address=branch_name,
                        isfc_code=int(isfc_code),
                        outstanding_bal=decimal.Decimal(out_bal),
                    )
                    auditlog.create("bank",bank_obj,user_id,"Bank updated.")
                    return JsonResponse({"msg": bank_name + " updated", "bank_id":bank_id, "code": 1}, status=200)
            else:
                exists_bank = Bank.objects.filter(name=bank_name).first()
                if exists_bank:
                    invalid_msg = bank_name + " is already exist."
                    return JsonResponse({'invalid_msg': invalid_msg, 'code': 0}, status=200)
                bank_obj = Bank.objects.create(
                    name=bank_name,
                    account_no=int(acc_no),
                    address=branch_name,
                    isfc_code=int(isfc_code),
                    outstanding_bal=decimal.Decimal(out_bal),
                )
                auditlog.create("bank",bank_obj.id,user_id,"Bank added")
                return JsonResponse({"msg": bank_obj.name + " created","bank_id":bank_obj.id, "code": 1}, status=200)
        else:
            if id:
                bank = Bank.objects.get(id=int(id))
                return render(request, "bank/transaction.html", {'data': bank})
            else:
                return render(request, "bank/transaction.html")

    except Exception as e:
        return JsonResponse({'invalid_msg': "Something went wrong" + str(e), 'code': 0}, status=200)

def delete_transactions(request):
    try:
        if request.method == 'POST':
            print(dict(request.POST.items()))
            ids = request.POST.get("ids").split(',')
            bank_name = ''
            # return JsonResponse({'msg': 'Something went wrong ', 'code': 0},status=200)
            for id in ids:
                bank_obj = Bank.objects.get(id=id)
                bank_name = bank_name + ',' + bank_obj.name if bank_name != '' else bank_name
                bank_obj.delete()
            return JsonResponse({'msg': 'Bank "' + bank_name + '" deleted.', 'code': 1}, status=200)
    except Exception as e:
        return JsonResponse({'msg': 'Something went wrong ', 'code': 0}, status=200)