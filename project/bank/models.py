from django.contrib import contenttypes
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

# Create your models here.
class Bank(models.Model):
    name = models.CharField(max_length=255)
    account_no = models.CharField(max_length=15)
    isfc_code = models.IntegerField()
    address = models.TextField()
    outstanding_bal = models.DecimalField(decimal_places=3, max_digits=7, default=0)
    created_at = models.DateTimeField(auto_now_add=True)

class Transaction(models.Model):
    amount = models.DecimalField(decimal_places=3, max_digits=7, default=0)
    type = models.BooleanField(default=True)
    descrption = models.TextField()
    bank = models.ForeignKey(Bank,on_delete=models.CASCADE)
    mop = models.CharField(max_length=255)
    party = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

