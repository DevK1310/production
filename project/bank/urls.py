from django.urls import path
from . import views
from bank.views import *

urlpatterns = [
    path('', views.home, name='aaaa'),
    path('banks', views.banks, name='banks'),
    path('bank', views.bank, name='bank'),
    path('bank/<int:id>/', views.bank, name='bank'),
    path('delete_banks', views.delete_banks, name='delete_banks'),
    path('transactions', views.transactions, name='transactions'),
    path('transaction', views.transaction, name='transaction'),
    path('transaction/<int:id>/', views.transaction, name='transection'),
    path('delete_transactions', views.delete_transactions, name='delete_transactions'),

]
