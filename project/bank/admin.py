from django.contrib import admin
from bank.models import Bank,Transaction
# Register your models here.

admin.site.register(Bank)
admin.site.register(Transaction)
